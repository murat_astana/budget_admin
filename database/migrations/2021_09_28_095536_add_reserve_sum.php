<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReserveSum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('reserve_sum');
        });

        Schema::create('reserve_transaction', function (Blueprint $table) {
            $table->id();
            $table->integer('total_sum')->default(0);
            $table->string('title')->nullable();
            $table->date('date_b')->nullable();
            $table->string('type')->default('from');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('reserve_sum');
        });

        Schema::dropIfExists('reserve_transaction');
    }
}
