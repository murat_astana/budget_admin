<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanWeekSpend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_week_spend', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cat_id');
            $table->integer('plan_summ')->default(0);
            $table->integer('fact_summ')->default(0);
            $table->date('date_monday')->nullable();
            $table->date('date_sunday')->nullable();
            $table->integer('date_year')->nullable();
            $table->integer('date_month')->nullable();
            $table->integer('date_week')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_week_spend');
    }
}
