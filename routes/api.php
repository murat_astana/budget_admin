<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('generate', [\App\Http\Controllers\Api\V1\AuthController::class, 'generate']);

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', [\App\Http\Controllers\Api\V1\AuthController::class, 'login']);

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\AuthController::class, 'getData']);
            Route::post('/', [\App\Http\Controllers\Api\V1\AuthController::class, 'update']);
        });

        Route::group(['prefix' => 'plan-spend'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\PlanSpendController::class, 'index']);
            Route::post('week/{item}', [\App\Http\Controllers\Api\V1\PlanSpendController::class, 'week']);
            Route::post('month/{item}', [\App\Http\Controllers\Api\V1\PlanSpendController::class, 'month']);
        });

        Route::group(['prefix' => 'notebook'], function () {
            Route::get('list/day', [\App\Http\Controllers\Api\V1\NotebookController::class, 'day']);
            Route::get('list/week', [\App\Http\Controllers\Api\V1\NotebookController::class, 'week']);
            Route::get('/', [\App\Http\Controllers\Api\V1\NotebookController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\NotebookController::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\NotebookController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\NotebookController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\NotebookController::class, 'destroy']);
        });

        Route::group(['prefix' => 'gift'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\GiftContoller::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\GiftContoller::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\GiftContoller::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\GiftContoller::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\GiftContoller::class, 'destroy']);
        });

        Route::group(['prefix' => 'conclusion'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\ConclusionController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\ConclusionController::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\ConclusionController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\ConclusionController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\ConclusionController::class, 'destroy']);
        });

        Route::group(['prefix' => 'reserve-transaction'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\ReserveTransactionController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\ReserveTransactionController::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\ReserveTransactionController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\ReserveTransactionController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\ReserveTransactionController::class, 'destroy']);
        });

        Route::group(['prefix' => 'credit'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\CreditController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\CreditController::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\CreditController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\CreditController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\CreditController::class, 'destroy']);
        });

        Route::group(['prefix' => 'spend'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\SpendController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\SpendController::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\SpendController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\SpendController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\SpendController::class, 'destroy']);
        });

        Route::group(['prefix' => 'spendcat'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\SpendCatController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\SpendCatController::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\SpendCatController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\SpendCatController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\SpendCatController::class, 'destroy']);
        });

        Route::group(['prefix' => 'profit'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\ProfitController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\ProfitController::class, 'store']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\ProfitController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\ProfitController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\ProfitController::class, 'destroy']);
        });

    });

});
