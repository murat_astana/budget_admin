<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Credit;
use Illuminate\Http\Request;

class CreditController extends Controller
{
    function index(Request $request){
        return Credit::latest()->get();
    }

    function store(Request $request){
        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        return Credit::create($data);
    }

    function update(Request $request, Credit $item){
        $item->update($request->all());

        return $item;
    }

    function show(Request $request, Credit $item){
        return $item;
    }

    function destroy(Request $request, Credit $item){
        $item->delete();

        return true;
    }


}
