<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Conclusion;
use Illuminate\Http\Request;

class ConclusionController extends Controller
{
    function index(Request $request){
        return Conclusion::latest()->get();
    }

    function store(Request $request){
        $data = $request->all();

        return Conclusion::create($data);
    }

    function update(Request $request, Conclusion $item){
        $item->update($request->all());

        return $item;
    }

    function show(Request $request, Conclusion $item){
        return $item;
    }

    function destroy(Request $request, Conclusion $item){
        $item->delete();

        return true;
    }


}
