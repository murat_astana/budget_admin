<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Notebook;
use App\Services\NotebookDayGenerator;
use Illuminate\Http\Request;

class NotebookController extends Controller
{
    function day(Request $request){
        $now = new \DateTime();

        if ($request->now_date && new \DateTime($request->now_date))
            $now = new \DateTime($request->now_date);

        return NotebookDayGenerator::gen($now);
    }

    function index(Request $request){
        return Notebook::filter($request)->orderBy('note_date', 'desc')->paginate();
    }

    function store(Request $request){
        $data = $request->all();
        $data['tag'] = 'tag';

        return Notebook::create($data);
    }

    function update(Request $request, Notebook $item){
        $item->update($request->all());

        return $item;
    }

    function show(Request $request, Notebook $item){
        return $item;
    }

    function destroy(Request $request, Notebook $item){
        $item->delete();

        return true;
    }


}
