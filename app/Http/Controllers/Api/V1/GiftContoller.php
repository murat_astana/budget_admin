<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\CreateGiftEvent;
use App\Events\DeleteGiftEvent;
use App\Events\UpdateGiftEvent;
use App\Http\Controllers\Controller;
use App\Models\Gift;
use Illuminate\Http\Request;

class GiftContoller extends Controller
{
    function index(Request $request){
        return Gift::latest()->get();
    }

    function store(Request $request){
        $data = $request->all();

        $item = Gift::create($data);

        event(new CreateGiftEvent($item));

        return $item;
    }

    function update(Request $request, Gift $item){
        $before_sum = $item->total_sum;

        $item->update($request->all());

        event(new UpdateGiftEvent($item, $before_sum));

        return $item;
    }

    function show(Request $request, Gift $item){
        return $item;
    }

    function destroy(Request $request, Gift $item){
        event(new DeleteGiftEvent($item));

        $item->delete();

        return true;
    }


}
