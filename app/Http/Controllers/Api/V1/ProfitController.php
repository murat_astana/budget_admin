<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\CreateProfitEvent;
use App\Events\DeleteProfitEvent;
use App\Events\UpdateProfitEvent;
use App\Http\Controllers\Controller;
use App\Models\Profit;
use Illuminate\Http\Request;

class ProfitController extends Controller
{
    function index(Request $request){
        return Profit::latest()->get();
    }

    function store(Request $request){
        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        $item = Profit::create($data);

        event(new CreateProfitEvent($item));

        return $item;
    }

    function update(Request $request, Profit $item){
        $before_sum = $item->total_sum;

        $item->update($request->all());

        event(new UpdateProfitEvent($item, $before_sum));

        return $item;
    }

    function show(Request $request, Profit $item){
        return $item;
    }

    function destroy(Request $request, Profit $item){

        event(new DeleteProfitEvent($item));

        $item->delete();

        return true;
    }


}
