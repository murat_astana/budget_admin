<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\GeneratePlanMonth;
use App\Services\GeneratePlanWeek;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    function generate(){
        GeneratePlanMonth::gen(new \DateTime());
        GeneratePlanWeek::gen(new \DateTime());
    }

    function login (Request $request){
        if (User::where(['email' => 'hmurich'])->count() == 0) {
            $admin = User::create([
                'email' => 'hmurich',
                'password' => Hash::make('346488'),
                'name' => 'Murat',
                'active' => 1
            ]);
        }

        if (!Auth::attempt(['email' => $request->input('login'), 'password' => $request->input('password')]) )
            return $this->false('Wrong credentials');

        $user = Auth::user();

        return [
            'data' => $user,
            'token' =>  $user->createToken('main')->plainTextToken
        ];
    }

    function logout(Request $request){
        $request->user()->tokens()->delete();

        return true;
    }

    function getData(Request $request){
        return $request->user();
    }

    function update(Request $request){
        $user = $request->user();

        if ($request->zp)
            $user->zp = $request->zp;

        if ($request->current_sum)
            $user->current_sum = $request->current_sum;

        if ($request->reserve_sum)
            $user->reserve_sum = $request->reserve_sum;

        $user->save();

        return $user;
    }
}
