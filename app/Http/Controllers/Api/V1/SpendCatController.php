<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\SpendCat;
use Illuminate\Http\Request;

class SpendCatController extends Controller
{
    function index(Request $request){
        return SpendCat::latest()->get();
    }

    function store(Request $request){
        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        return SpendCat::create($data);
    }

    function update(Request $request, SpendCat $item){
        $item->update($request->all());

        return $item;
    }

    function show(Request $request, SpendCat $item){
        return $item;
    }

    function destroy(Request $request, SpendCat $item){
        $item->delete();

        return true;
    }


}
