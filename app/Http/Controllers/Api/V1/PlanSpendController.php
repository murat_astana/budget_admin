<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\PlanMonthSpend;
use App\Models\PlanWeekSpend;
use App\Models\SpendCat;
use App\Models\User;
use Illuminate\Http\Request;

class PlanSpendController extends Controller{
    private $now;

    function __construct(){
        $this->now = new \DateTime();
    }
	
	function modifyThisDate(){
		if ($this->now->format('N') == 1)
			return;
		
		$day_diff = $this->now->format('N') - 1;
        $this->now->modify('-'.$day_diff.' day');
		
	}

    function index(Request $request){
        if ($request->now_date && new \DateTime($request->now_date))
            $this->now = new \DateTime($request->now_date);
		
		$this->modifyThisDate();

        $data = [];
        $data['month'] = $this->getMonthStat();
        $data['current_week'] = $this->getCurrentWeekPlanStat();
        $data['week'] = $this->getWeekPlanStat();
        $data['user'] = $this->getUserStat();
        $data['current_week_num'] = $this->getCurrentWeekNum();
        $data['before_week_date'] = $this->calcBeforeWeekDate();
        $data['after_week_date'] = $this->calcAfterWeekDate();
        $data['current_week_date'] = $this->now->format('Y-m-d');

        return $data;
    }

    function week(Request $request, PlanWeekSpend $item){
        $item->update($request->all());

        return $item;
    }

    function month(Request $request, PlanMonthSpend $item){
        $item->update($request->all());

        return $item;
    }

    private function calcBeforeWeekDate(){
        $date = clone $this->now;
        $date->modify('-1 week');

        return $date->format('Y-m-d');
    }

    private function calcAfterWeekDate(){
        $date = clone $this->now;
        $date->modify('+1 week');
		
        return $date->format('Y-m-d');
    }

    private function getCurrentWeekNum(){
        return  PlanWeekSpend::where('date_monday', "<=", $this->now->format('Y-m-d'))
            ->where('date_sunday', '>=', $this->now->format('Y-m-d'))->first()->date_week;

    }

    private function getUserStat(){
        $ar = [];
        $ar['zp'] = User::first()->zp;
        $ar['current_sum'] = User::first()->current_sum;
        $ar['reserve_sum'] = User::first()->reserve_sum;
        $ar['main_total_spend'] = SpendCat::sum('sum_in_month');
        $ar['balans_1'] = $ar['zp'] - $ar['main_total_spend'];
        $ar['current_total_plan'] = PlanMonthSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])
                                        ->sum('plan_summ') +
                                    PlanWeekSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])
                                        ->sum('plan_summ');
        $ar['current_total_fact'] = PlanMonthSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])
                                        ->sum('fact_summ') +
                                    PlanWeekSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])
                                        ->sum('fact_summ');

        $ar['need_to_finish'] = $this->calcNeedToFinish();

        return $ar;
    }

    private function calcNeedToFinish(){
        $current_week = PlanWeekSpend::where('date_monday', "<=", $this->now->format('Y-m-d'))
                                        ->where('date_sunday', '>=', $this->now->format('Y-m-d'))
                                        ->first();

        $current_week_total = PlanWeekSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])
                                        ->where('date_week', $current_week->date_week)->sum('plan_summ');

        $c = 0;

        $c += PlanWeekSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])
                            ->where('date_week', '>', $current_week->date_week)->sum('plan_summ');
        $c += PlanMonthSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])
                            ->where('fact_summ', 0)->sum('plan_summ');

        $c += ($current_week_total / 7) * (7 - $this->now->format('N'));

        return round($c);
    }

    private function getMonthStat(){
        $ar = [];
        $ar['items'] = PlanMonthSpend::where([
                                        'date_year' => $this->now->format('Y'),
                                        'date_month' => $this->now->format('n')])->with('relCat')->get();
        $ar['plan_sum'] = PlanMonthSpend::where([
                                        'date_year' => $this->now->format('Y'),
                                        'date_month' => $this->now->format('n')])->sum('plan_summ');
        $ar['fact_sum'] = PlanMonthSpend::where([
                                        'date_year' => $this->now->format('Y'),
                                        'date_month' => $this->now->format('n')])->sum('fact_summ');

        $ar['balans'] = $ar['plan_sum'] - $ar['fact_sum'];

        return $ar;
    }

    private function getWeekPlanStat(){
        $ar_cat = SpendCat::pluck('name', 'id')->toArray();

        $max_week = PlanWeekSpend::where(['date_year' => $this->now->format('Y'),
                                            'date_month' => $this->now->format('n')])->max('date_week');

        $weeks = [];

        $total = [
            'data' => [],
            'main' => [
                'plan' => 0,
                'fact' => 0,
                'balans' => 0
            ]
        ];

        $data = [];
        for ($counter = 1; $counter <= $max_week; $counter++){
            $items = PlanWeekSpend::where([
                            'date_year' => $this->now->format('Y'),
                            'date_month' => $this->now->format('n'),
                            'date_week' => $counter])->get();


            $total['data'][$counter] = [
                'plan' => 0,
                'fact' => 0,
                'balans' => 0
            ];

            foreach ($items as $i){
                if (!isset($weeks[$counter])){
                    $weeks[$counter]['b'] = (new \DateTime($i->date_monday))->format('d.m');
                    $weeks[$counter]['e'] = (new \DateTime($i->date_sunday))->format('d.m');
                }

                if (!isset($data[$i->cat_id])) {
                    $data[$i->cat_id] = [];
                    $data[$i->cat_id]['name'] = $ar_cat[$i->cat_id];
                    $data[$i->cat_id]['data'] = [];
                    $data[$i->cat_id]['total'] = [
                        'plan' => 0,
                        'fact' => 0,
                        'balans' => 0
                    ];
                }

                if (!isset($data[$i->cat_id]['data'][$counter]))
                    $data[$i->cat_id]['data'][$counter] = [];

                // calc by week
                $data[$i->cat_id]['data'][$counter]['id'] = $i->id;
                $data[$i->cat_id]['data'][$counter]['cat_id'] = $i->cat_id;
                $data[$i->cat_id]['data'][$counter]['date_b'] = $i->date_monday;
                $data[$i->cat_id]['data'][$counter]['date_e'] = $i->date_sunday;
                $data[$i->cat_id]['data'][$counter]['plan'] = $i->plan_summ;
                $data[$i->cat_id]['data'][$counter]['fact'] = $i->fact_summ;
                $data[$i->cat_id]['data'][$counter]['balans'] = $i->plan_summ - $i->fact_summ;

                // calc total by row
                $data[$i->cat_id]['total']['plan'] += $i->plan_summ;
                $data[$i->cat_id]['total']['fact'] += $i->fact_summ;
                $data[$i->cat_id]['total']['balans'] += $i->plan_summ - $i->fact_summ;

                // calc total by column
                $total['data'][$counter]['plan'] += $i->plan_summ;
                $total['data'][$counter]['fact'] += $i->fact_summ;
                $total['data'][$counter]['balans'] += $i->plan_summ - $i->fact_summ;
            }

            $total['main']['plan'] += $total['data'][$counter]['plan'];
            $total['main']['fact'] += $total['data'][$counter]['fact'];
            $total['main']['balans'] += $total['data'][$counter]['balans'];
        }

        return [
            'column' => $total,
            'row' => $data,
            'max_week' => $max_week,
            'weeks' => $weeks
        ];
    }

    private function getCurrentWeekPlanStat(){
		
        $ar = [];
        $ar['items'] = PlanWeekSpend::where('date_monday', "<=", $this->now->format('Y-m-d'))
                                    ->where('date_sunday', '>=', $this->now->format('Y-m-d'))
                                    ->with('relCat')->get();
        $ar['plan_sum'] = PlanWeekSpend::where('date_monday', "<=", $this->now->format('Y-m-d'))
                                    ->where('date_sunday', '>=', $this->now->format('Y-m-d'))
                                    ->sum('plan_summ');
        $ar['fact_sum'] = PlanWeekSpend::where('date_monday', "<=", $this->now->format('Y-m-d'))
                                    ->where('date_sunday', '>=', $this->now->format('Y-m-d'))
                                    ->sum('fact_summ');

        $ar['balans'] = $ar['plan_sum'] - $ar['fact_sum'];
        $ar['date'] = $this->now->format('Y-m-d');
		

        return $ar;
    }

}