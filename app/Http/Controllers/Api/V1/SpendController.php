<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\CreateSpendEvent;
use App\Events\DeleteSpendEvent;
use App\Events\UpdateSpendEvent;
use App\Http\Controllers\Controller;
use App\Models\Spend;
use Illuminate\Http\Request;

class SpendController extends Controller
{
    function index(Request $request){
        $items = Spend::filter($request)->orderBy('date_b', 'desc')->orderBy('id', 'desc')->with('relCat');
        if ($request->all == 1)
            return $items->get();

        return $items->paginate(24);
    }

    function store(Request $request){
        $data = $request->all();
        $data['user_id'] = $request->user()->id;

        $item = Spend::create($data);

        event(new CreateSpendEvent($item));

        return $item;
    }

    function update(Request $request, Spend $item){
        $before_sum = $item->total_sum;

        $item->update($request->all());

        event(new UpdateSpendEvent($item, $before_sum));

        return $item;
    }

    function show(Request $request, Spend $item){
        return $item;
    }

    function destroy(Request $request, Spend $item){
        event(new DeleteSpendEvent($item));

        $item->delete();

        return true;
    }


}
