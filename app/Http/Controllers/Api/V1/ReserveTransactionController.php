<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\CreateReserveTransactionEvent;
use App\Events\DeleteReserveTransactionEvent;
use App\Events\UpdateReserveTransactionEvent;
use App\Http\Controllers\Controller;
use App\Models\ReserveTransaction;
use Illuminate\Http\Request;

class ReserveTransactionController extends Controller
{
    function index(Request $request){
        return ReserveTransaction::latest()->get();
    }

    function store(Request $request){
        $data = $request->all();

        $item = ReserveTransaction::create($data);

        event(new CreateReserveTransactionEvent($item));

        return $item;
    }

    function update(Request $request, ReserveTransaction $item){
        $before_sum = $item->total_sum;

        $item->update($request->all());

        event(new UpdateReserveTransactionEvent($item, $before_sum));

        return $item;
    }

    function show(Request $request, ReserveTransaction $item){
        return $item;
    }

    function destroy(Request $request, ReserveTransaction $item){
        event(new DeleteReserveTransactionEvent($item));

        $item->delete();

        return true;
    }


}
