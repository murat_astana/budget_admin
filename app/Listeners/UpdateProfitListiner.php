<?php

namespace App\Listeners;

use App\Events\UpdateProfitEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateProfitListiner
{

    public function handle(UpdateProfitEvent $event)
    {
        $el = $event->el;
        $sum = $el->total_sum - $event->before_sum;

        $user = User::first();
        $user->update(['current_sum' => ($user->current_sum + $sum)]);
    }
}
