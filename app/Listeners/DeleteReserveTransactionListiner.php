<?php

namespace App\Listeners;

use App\Events\DeleteReserveTransactionEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DeleteReserveTransactionListiner
{
    public function handle(DeleteReserveTransactionEvent $event)
    {
        $el = $event->el;

        $user = User::first();

        if ($el->type == 'from') {
            $user->reserve_sum = $user->reserve_sum + $el->total_sum;
            $user->current_sum = $user->current_sum - $el->total_sum;
        } else {
            $user->reserve_sum = $user->reserve_sum - $el->total_sum;
            $user->current_sum = $user->current_sum + $el->total_sum;
        }

        $user->save();
    }
}
