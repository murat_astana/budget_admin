<?php

namespace App\Listeners;

use App\Events\CreateReserveTransactionEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateReserveTransactionListiner
{

    public function handle(CreateReserveTransactionEvent $event)
    {
        $el = $event->el;

        $user = User::first();

        if ($el->type == 'from') {
            $user->reserve_sum = $user->reserve_sum - $el->total_sum;
            $user->current_sum = $user->current_sum + $el->total_sum;
        } else {
            $user->reserve_sum = $user->reserve_sum + $el->total_sum;
            $user->current_sum = $user->current_sum - $el->total_sum;
        }

        $user->save();

    }
}
