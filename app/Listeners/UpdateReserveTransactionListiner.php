<?php

namespace App\Listeners;

use App\Events\UpdateReserveTransactionEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateReserveTransactionListiner
{

    public function handle(UpdateReserveTransactionEvent $event)
    {
        $el = $event->el;
        $before = $event->before;

        $user = User::first();

        if ($el->type == 'from') {
            $user->reserve_sum = $user->reserve_sum + $before;
            $user->reserve_sum = $user->reserve_sum - $el->total_sum;

            $user->current_sum = $user->current_sum - $before;
            $user->current_sum = $user->current_sum + $el->total_sum;
        } else {
            $user->reserve_sum = $user->reserve_sum - $before;
            $user->reserve_sum = $user->reserve_sum + $el->total_sum;

            $user->current_sum = $user->current_sum + $before;
            $user->current_sum = $user->current_sum - $el->total_sum;
        }

        $user->save();
    }
}
