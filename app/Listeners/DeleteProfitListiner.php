<?php

namespace App\Listeners;

use App\Events\DeleteProfitEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DeleteProfitListiner
{
    public function handle(DeleteProfitEvent $event)
    {
        $el = $event->el;

        $user = User::first();
        $user->update(['current_sum' => ($user->current_sum - $el->total_sum)]);
    }
}
