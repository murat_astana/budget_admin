<?php

namespace App\Listeners;

use App\Events\UpdateSpendEvent;
use App\Models\PlanMonthSpend;
use App\Models\PlanWeekSpend;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateSpendListiner
{
    public function handle(UpdateSpendEvent $event)
    {
        $el = $event->el;
        $before_sum = $event->before_sum;
        if (!$el->relCat)
            return true;


        $sum = $el->total_sum - $before_sum;

        $cat = $el->relCat;

        $user = User::first();
        $user->update(['current_sum' => ($user->current_sum - $sum)]);

        if ($cat->sum_in_week > 0)
            $plan = PlanWeekSpend::where('cat_id', $el->cat_id)->where('date_monday', '<=', $el->date_b)->where('date_sunday', '>=', $el->date_b)->first();
        else {
            $date = new \DateTime($el->date_b);
            $plan = PlanMonthSpend::where('cat_id', $el->cat_id)->where('date_year', $date->format('Y'))->where('date_month', $date->format('n'))->first();
        }

        if (!$plan)
            return true;

        $plan->update(['fact_summ' => ($plan->fact_summ + $sum)]);
    }
}
