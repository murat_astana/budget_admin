<?php

namespace App\Listeners;

use App\Events\UpdateGiftEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateGiftListiner
{
    public function handle(UpdateGiftEvent $event)
    {
        $el = $event->el;
        $sum = $el->total_sum - $event->before_sum;

        $user = User::first();
        $user->update(['reserve_sum' => ($user->current_sum - $sum)]);
    }
}
