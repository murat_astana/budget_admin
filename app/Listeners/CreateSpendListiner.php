<?php

namespace App\Listeners;

use App\Events\CreateSpendEvent;
use App\Models\PlanMonthSpend;
use App\Models\PlanWeekSpend;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateSpendListiner
{
    public function handle(CreateSpendEvent $event)
    {
        $el = $event->el;
        if (!$el->relCat)
            return true;

        $cat = $el->relCat;

        $user = User::first();
        $user->update(['current_sum' => ($user->current_sum - $el->total_sum)]);

        if ($cat->sum_in_week > 0)
            $plan = PlanWeekSpend::where('cat_id', $el->cat_id)->where('date_monday', '<=', $el->date_b)->where('date_sunday', '>=', $el->date_b)->first();
        else {
            $date = new \DateTime($el->date_b);
            $plan = PlanMonthSpend::where('cat_id', $el->cat_id)->where('date_year', $date->format('Y'))->where('date_month', $date->format('n'))->first();
        }

        if (!$plan)
            return true;

        $plan->update(['fact_summ' => ($plan->fact_summ + $el->total_sum)]);
    }
}
