<?php

namespace App\Listeners;

use App\Events\DeleteGiftEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DeleteGiftListiner
{
    public function handle(DeleteGiftEvent $event)
    {
        $el = $event->el;

        $user = User::first();
        $user->update(['reserve_sum' => ($user->reserve_sum + $el->total_sum)]);
    }
}
