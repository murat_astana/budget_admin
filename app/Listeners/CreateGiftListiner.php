<?php

namespace App\Listeners;

use App\Events\CreateGiftEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateGiftListiner
{

    public function handle(CreateGiftEvent $event)
    {
        $el = $event->el;

        $user = User::first();
        $user->update(['reserve_sum' => ($user->reserve_sum - $el->total_sum)]);
    }
}
