<?php

namespace App\Listeners;

use App\Events\CreateProfitEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateProfitListiner
{
    public function handle(CreateProfitEvent $event)
    {
        $el = $event->el;

        $user = User::first();
        $user->update(['current_sum' => ($user->current_sum + $el->total_sum)]);
    }
}
