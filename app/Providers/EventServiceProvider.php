<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\CreateSpendEvent::class => [
            \App\Listeners\CreateSpendListiner::class,
        ],
        \App\Events\UpdateSpendEvent::class => [
            \App\Listeners\UpdateSpendListiner::class,
        ],
        \App\Events\DeleteSpendEvent::class => [
            \App\Listeners\DeleteSpendListiner::class,
        ],


        \App\Events\CreateProfitEvent::class => [
            \App\Listeners\CreateProfitListiner::class,
        ],
        \App\Events\UpdateProfitEvent::class => [
            \App\Listeners\UpdateProfitListiner::class,
        ],
        \App\Events\DeleteProfitEvent::class => [
            \App\Listeners\DeleteProfitListiner::class,
        ],


        \App\Events\CreateReserveTransactionEvent::class => [
            \App\Listeners\CreateReserveTransactionListiner::class,
        ],
        \App\Events\UpdateReserveTransactionEvent::class => [
            \App\Listeners\UpdateReserveTransactionListiner::class,
        ],
        \App\Events\DeleteReserveTransactionEvent::class => [
            \App\Listeners\DeleteReserveTransactionListiner::class,
        ],


        \App\Events\CreateGiftEvent::class => [
            \App\Listeners\CreateGiftListiner::class,
        ],
        \App\Events\UpdateGiftEvent::class => [
            \App\Listeners\UpdateGiftListiner::class,
        ],
        \App\Events\DeleteGiftEvent::class => [
            \App\Listeners\DeleteGiftListiner::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
