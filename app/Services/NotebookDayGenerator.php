<?php
namespace App\Services;

use App\Models\Notebook;
use DateTime;

class NotebookDayGenerator {
    private $date;
    public $data;

    public function __construct(DateTime $date) {
        $this->date = $date;
        $this->now = new DateTime();
        $this->calc();
        $this->calcBefore();
        $this->calcAfter();
    }

    static function gen(DateTime $date){
        $el = new static($date);
        return $el->data;
    }

    private function calcBefore(){
        $this->data['before_date'] = null;

        $sunday = new DateTime($this->data['sunday']);
        $sunday->modify('-1 week');

        $this->data['before_date'] = $sunday->format('Y-m-d');
    }

    private function calcAfter(){
        $this->data['after_date'] = null;

        $sunday = new DateTime($this->data['sunday']);
        $sunday->modify('+1 week');

        $this->data['after_date'] = $sunday->format('Y-m-d');
    }

    private function calc(){
        $ar = [];

        $d = clone $this->date;
        if ($d->format('N') != 7)
            $d->modify('Next Sunday');

        $this->data['sunday'] = $d->format('Y-m-d');
        $this->data['now'] = $this->now->format('Y-m-d');

        for ($i = 1; $i <= 7; $i ++){
            if($d->format('Y-m-d') > $this->now->format('Y-m-d')) {
                $d->modify('-1 day');
                continue;
            }

            $ar[] = [
                'date' => $d->format('Y-m-d'),
                'good' => Notebook::where('note_date', $d->format('Y-m-d'))->where('day_type', 'day')->where('note_type', 'good')->latest()->get(),
                'bad' => Notebook::where('note_date', $d->format('Y-m-d'))->where('day_type', 'day')->where('note_type', 'bad')->latest()->get(),
                'main' => Notebook::where('note_date', $d->format('Y-m-d'))->where('day_type', 'day')->where('note_type', 'main')->latest()->get()
            ];

            $d->modify('-1 day');
        }

        $this->data['data'] = $ar;

    }


}