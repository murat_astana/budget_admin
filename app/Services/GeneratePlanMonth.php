<?php
namespace App\Services;

use App\Models\PlanMonthSpend;
use App\Models\SpendCat;
use DateTime;

class GeneratePlanMonth {
    private $now;

    public function __construct(DateTime $date) {
        $this->now = $date;
        $this->check();
    }

    static function gen(DateTime $date){
        $el = new static($date);
    }

    private function check(){
        if (PlanMonthSpend::where('date_year', $this->now->format('Y'))->where('date_month', $this->now->format('m'))->count() > 0)
            return 0;

        $cats = SpendCat::where('active', 1)->where('sum_in_week', 0)->get();

        foreach ($cats as $c){
            PlanMonthSpend::create([
                'cat_id' => $c->id,
                'plan_summ' => $c->sum_in_month,
                'fact_summ' => 0,
                'date_year' => $this->now->format('Y'),
                'date_month' => $this->now->format('m')
            ]);
        }
    }

}