<?php
namespace App\Services;

use App\Events\CreateSpendEvent;
use App\Models\PlanMonthSpend;
use App\Models\PlanWeekSpend;
use App\Models\Spend;
use DateTime;

class CalcPlanFactSum {
    private $now;

    public function __construct(DateTime $date) {
        $this->now = $date;
        $this->check();
    }

    static function gen(DateTime $date){
        $el = new static($date);
    }

    private function check(){
        PlanWeekSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])->update(['fact_summ' => 0]);
        PlanMonthSpend::where(['date_year' => $this->now->format('Y'), 'date_month' => $this->now->format('n')])->update(['fact_summ' => 0]);

        $items = Spend::where('date_b', '>=', '2021-'.$this->now->format('m').'-01')
                        ->where('date_b', '<=', '2021-'.$this->now->format('m').'-30')->get();


        foreach ($items  as $item){
            event(new CreateSpendEvent($item));
        }


    /*
        $current_month = PlanWeekSpend::where('date_monday', "<=", $this->now->format('Y-m-d'))
            ->where('date_sunday', '>=', $this->now->format('Y-m-d'))->first();

    */
    }

}