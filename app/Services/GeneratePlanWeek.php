<?php
namespace App\Services;

use App\Models\PlanWeekSpend;
use App\Models\SpendCat;
use DateTime;

class GeneratePlanWeek {
    private $now;
    private $date_b;

    public function __construct(DateTime $date) {
        $this->now = $date;
        $this->check();
    }

    static function gen(DateTime $date){
        $el = new static($date);
    }

    private function check(){
        if (PlanWeekSpend::where('date_year', $this->now->format('Y'))->where('date_month', $this->now->format('m'))->count() > 0)
            return 0;

        $this->calcMonday();

        $cats = SpendCat::where('active', 1)->where('sum_in_week', ">", 0)->get();

        $monday = clone $this->date_b;
        for ($i = 1; $i <= 5; $i++){
            $sunday = clone $monday;
            $sunday->modify('+6 day');

            foreach ($cats as $c){
                PlanWeekSpend::create([
                    'cat_id' => $c->id,
                    'plan_summ' => $c->sum_in_week,
                    'fact_summ' => 0,
                    'date_monday' => $monday->format('Y-m-d'),
                    'date_sunday' => $sunday->format('Y-m-d'),
                    'date_year' => $monday->format('Y'),
                    'date_month' => $monday->format('m'),
                    'date_week' => $i
                ]);
            }

            $monday->modify('+7 day');
            if ($sunday->format('m') != $this->now->format('m') || $monday->format('m') != $this->now->format('m'))
                break;
        }
    }

    private function calcMonday(){
        $this->date_b = clone $this->now;
        $this->date_b->modify('first day of this month');
        $this->date_b = $this->date_b->setISODate($this->date_b->format("Y"), $this->date_b->format("W"), 1);
        if ($this->date_b->format('m') != $this->now->format('m'))
            $this->date_b->modify('+7 days');
    }
}