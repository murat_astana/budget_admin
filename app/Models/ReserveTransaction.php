<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReserveTransaction extends Model
{
    use HasFactory;
    protected $table = 'reserve_transaction';
    protected $fillable = ['total_sum', 'title', 'date_b', 'type'];

}
