<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanWeekSpend extends Model
{
    use HasFactory;
    protected $table = 'plan_week_spend';
    protected $fillable = ['cat_id', 'plan_summ', 'fact_summ',
        'date_monday', 'date_sunday', 'date_year', 'date_month',
        'date_week'];

    function relCat(){
        return $this->belongsTo(SpendCat::class, 'cat_id');
    }
}
