<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpendCat extends Model
{
    use HasFactory;
    protected $table = 'spend_cat';
    protected $fillable = ['name', 'note', 'active', 'sum_in_week', 'sum_in_month', 'user_id'];

    function relSpend(){
        return $this->hasMany(Spend::class, 'cat_id');
    }

}
