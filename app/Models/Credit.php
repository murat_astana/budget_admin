<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    use HasFactory;
    protected $table = 'credits';
    protected $fillable = ['name', 'active', 'total_sum', 'need_to_close_sum', 'sum_month', 'user_id'];

}
