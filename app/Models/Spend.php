<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Spend extends Model
{
    use HasFactory;
    protected $table = 'spends';
    protected $fillable = ['name', 'cat_id', 'total_sum', 'user_id', 'date_b'];

    function relCat(){
        return $this->belongsTo(SpendCat::class, 'cat_id');
    }

    function scopeFilter($query, Request $request){
        if ($request->has('name') && trim($request->name))
            $query->where('name', 'like', '%'.$request->name.'%');

        if ($request->has('cat_id') && $request->cat_id)
            $query->where('cat_id', $request->cat_id);

        if ($request->has('date_b') && $request->date_b)
            $query->where('date_b', '>=', $request->date_b);

        if ($request->has('date_e') && $request->date_e)
            $query->where('date_b', '<=', $request->date_e);
    }

}
