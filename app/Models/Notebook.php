<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Notebook extends Model
{
    use HasFactory;
    protected $table = 'notebook';
    protected $fillable = ['note_date', 'note', 'note_type', 'day_type', 'tag'];

    CONST NOTE_TYPE_GOOD = 'good';
    CONST NOTE_TYPE_BAD = 'bad';
    CONST NOTE_TYPE_MAIN = 'main';

    CONST DAY_TYPE_DAY = 'day';
    CONST DAY_TYPE_WEEK = 'week';
    CONST DAY_TYPE_MONTH = 'month';

    function scopeFilter($query, Request $request){
        if ($request->has('note_type') && in_array($request->note_type, static::getNoteTypeAr()))
            $query->where('note_type', $request->note_type);

        if ($request->has('day_type') && in_array($request->day_type, static::getDayTypeAr()))
            $query->where('day_type', $request->day_type);

        if ($request->has('note_date') && $date = new \DateTime($request->note_date))
            $query->where('note_date', $date->format('Y-m-d'));

        if ($request->has('date_b') && $date = new \DateTime($request->date_b))
            $query->where('note_date', '>=', $date->format('Y-m-d'));

        if ($request->has('date_e') && $date = new \DateTime($request->date_e))
            $query->where('note_date', '<=', $date->format('Y-m-d'));

        if ($request->has('note') && $request->note)
            $query->where('note', 'like', '%'.$request->note.'%');

    }

    static function getNoteTypeAr(){
        return [
            self::NOTE_TYPE_BAD,
            self::NOTE_TYPE_GOOD,
            self::NOTE_TYPE_MAIN
        ];
    }

    static function getDayTypeAr(){
        return [
            self::DAY_TYPE_DAY,
            self::DAY_TYPE_WEEK,
            self::DAY_TYPE_MONTH
        ];
    }

}
