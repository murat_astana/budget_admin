<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanMonthSpend extends Model
{
    use HasFactory;
    protected $table = 'plan_month_spend';
    protected $fillable = ['cat_id', 'plan_summ', 'fact_summ', 'date_year', 'date_month'];

    function relCat(){
        return $this->belongsTo(SpendCat::class, 'cat_id');
    }

}
