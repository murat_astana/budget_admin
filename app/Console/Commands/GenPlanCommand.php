<?php

namespace App\Console\Commands;

use App\Services\GeneratePlanMonth;
use App\Services\GeneratePlanWeek;
use Illuminate\Console\Command;

class GenPlanCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        GeneratePlanMonth::gen(new \DateTime());
        GeneratePlanWeek::gen(new \DateTime());
    }
}
